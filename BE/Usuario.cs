﻿using BE.Permisos;
using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Usuario : IUsuario
    {
        private IList<IPermiso> _permisos;
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Usuario()
        {
            _permisos = new List<IPermiso>();
        }
        public IList<IPermiso> Permisos
        {
            get
            {
                return _permisos;
            }
        }
    }
}
