﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
namespace BE.Permisos
{
    public abstract class Componente : IPermiso
    {
        public string Nombre { get; set; }
        public int Id { get; set; }
        public abstract IList<IPermiso> Hijos { get; }
        public abstract void AgregarHijo(Componente c);
        public abstract void VaciarHijos();
        public TipoPermiso Permiso { get; set; }

        public override string ToString()
        {
            return Nombre;
        }

        public void AgregarPermiso(IPermiso p)
        {
            throw new NotImplementedException();
        }

        public void QuitarPermiso(IPermiso p)
        {
            throw new NotImplementedException();
        }

        public IList<IPermiso> ObtenerHijos()
        {
            return Hijos;
        }
    }
}
