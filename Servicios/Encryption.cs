﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public static class Encryption
    {
        public static byte[] hashValue = { 59, 4, 248, 102, 77, 97, 142, 201, 210, 12, 224, 93, 25, 41, 100, 197, 213, 134, 130, 135 };
        public static byte[] signedHashValue;
        public static RSAParameters rsaKeyInfo;
        public static RSA rsa;
      
        public static string Hash(string value)
        {
            var md5 = new MD5CryptoServiceProvider();
            var md5data = md5.ComputeHash(Encoding.ASCII.GetBytes(value));
            return (new ASCIIEncoding()).GetString(md5data);
        }

        public static void CrearFirma(string container) 
        {
            //The hash value to sign.
            //byte[] hashValue = { 59, 4, 248, 102, 77, 97, 142, 201, 210, 12, 224, 93, 25, 41, 100, 197, 213, 134, 130, 135 };

            //The value to hold the signed value.
            //byte[] signedHashValue;

            //Generate a public/private key pair.
            rsa = RSA.Create();

            GuardarClaveContainer(container);
        }
        //Create an RSAPKCS1SignatureFormatter object and pass it the
        //RSA instance to transfer the private key.
        public static void Firmar() 
        { 
        RSAPKCS1SignatureFormatter rsaFormatter = new RSAPKCS1SignatureFormatter(rsa);

        //Set the hash algorithm to SHA1.
        rsaFormatter.SetHashAlgorithm("SHA1");

        //Create a signature for hashValue and assign it to
        //signedHashValue.
        signedHashValue = rsaFormatter.CreateSignature(hashValue);
        }

        public static void verificarFirma(string container)
        {

            rsa = null;
            BuscarClaveContainer(container);
            Firmar();

            RSAPKCS1SignatureDeformatter rsaDeformatter = new RSAPKCS1SignatureDeformatter(rsa);
            rsaDeformatter.SetHashAlgorithm("SHA1");
            if (rsaDeformatter.VerifySignature(hashValue, signedHashValue))
            {
                Console.WriteLine("The signature is valid.");
            }
            else
            {
                Console.WriteLine("The signature is not valid.");
            }
        }

        private static void GuardarClaveContainer(string container)
        {
            // Create the CspParameters object and set the key container
            // name used to store the RSA key pair.
            var parameters = new CspParameters
            {
                KeyContainerName = container
            };

            // Create a new instance of RSACryptoServiceProvider that accesses
            // the key container MyKeyContainerName.
            rsa = new RSACryptoServiceProvider(parameters);

        }

        private static void BuscarClaveContainer(string container)
        {
            // Create the CspParameters object and set the key container
            // name used to store the RSA key pair.
            var parameters = new CspParameters
            {
                KeyContainerName = container
            };

            // Create a new instance of RSACryptoServiceProvider that accesses
            // the key container MyKeyContainerName.
            rsa = new RSACryptoServiceProvider(parameters);

        }

        private static void BorrarClaveContainer(string container)
        {
            // Create the CspParameters object and set the key container
            // name used to store the RSA key pair.
            var parameters = new CspParameters
            {
                KeyContainerName = container
            };

            // Create a new instance of RSACryptoServiceProvider that accesses
            // the key container.
            rsa = new RSACryptoServiceProvider(parameters)
            {
                // Delete the key entry in the container.
                PersistKeyInCsp = false
            };

            // Call Clear to release resources and delete the key from the container.
            rsa.Clear();
        }
    }
}
