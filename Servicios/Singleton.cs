﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public class Singleton
    {
        private static Singleton _session;

        public IUsuario Usuario { get; set; }
        public DateTime InicioSesion { get; set; }

        private Singleton()
        {
            //para asegurarnos de que no se cree una instancia del objeto
        }

        public static Boolean IsLogged()
        {
            if (_session == null)
                return false;
            else
            {
                return true;
            }
        }
        public static Singleton getInstance
        {
            get
            {
                if (_session == null) { throw new Exception("Sesión no inciada"); }
                return _session;
            }
        }

        private static Object _lock = new object();
        public static void Login(IUsuario usuario)
        {

            lock (_lock) 
            {
                if (_session == null)
                {
                    _session = new Singleton();
                    _session.Usuario = usuario;
                    _session.InicioSesion = DateTime.Now;

                }
                else
                {
                    throw new Exception("Sesión ya iniciada");
                }
            }
        }

        public static void Logout()
        {
            lock (_lock) 
            {
                if (_session != null)
                {
                    _session = null;
                }
                else
                {
                    throw new Exception("Sesión no iniciada");
                }
            }
        }
    }
}
