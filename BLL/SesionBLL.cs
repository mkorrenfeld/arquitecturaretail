﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Servicios;
using DAL;
using BE;
using BE.Permisos;

namespace BLL
{
    public class SesionBLL
    {
        UsuarioDAL _usrioDAL;
        private List<Usuario> usuarios; 

        public SesionBLL()
        {
            _usrioDAL = new UsuarioDAL();
            usuarios = _usrioDAL.GetAll();
        }
        public void Login(string username, string password)
        {

            if (Singleton.IsLogged())
                throw new Exception("Ya hay una sesión iniciada");

            var user = usuarios.Where(usr => usr.Username.Equals(username)).FirstOrDefault();
            if (user == null) throw new Exception("Invalid Username");

            if (!Encryption.Hash(password).Equals(user.Password))
                throw new Exception("Invalid Password");
            else
            {
                (new PermisoDAL()).FillUserComponents(user);
                Singleton.Login(user);
            }
        }

        public void Logout()
        {
            if (Singleton.getInstance == null)
                throw new Exception("No has iniciado sesión");

            Singleton.Logout();
        }

        private bool IsInRoleRecursivo(Componente p, Enum tipoPermiso, bool valid)
        {

            foreach (var item in p.ObtenerHijos())
            {
                if (item is Patente && ((Patente)item).Permiso.Equals(tipoPermiso))
                {
                    valid = true;
                }
                else
                {
                    valid = IsInRoleRecursivo((Componente)item, tipoPermiso, valid);
                }
            }
            return valid;
        }

        public bool IsInRole(Enum tipoPermiso)
        {
            if (!Singleton.IsLogged()) return false;

            bool valid = false;
            foreach (var p in Singleton.getInstance.Usuario.Permisos)
            {
                if (p is Patente && ((Patente)p).Permiso.Equals(tipoPermiso))
                {
                    valid = true;
                }
                else
                {
                    valid = IsInRoleRecursivo((Componente)p, tipoPermiso, valid);
                }
            }

            return valid;
        }

        public string devolverNombreSesion()
        {
            return Singleton.getInstance.Usuario.Username.ToString();
        }
    }
}
