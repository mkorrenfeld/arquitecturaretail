﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI
{
    public partial class frmLogin : Form
    {
        SesionBLL _sesionBLL;
        public frmLogin()
        {
            InitializeComponent();
            _sesionBLL = new SesionBLL();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            _sesionBLL.Login(this.txtUser.Text, this.txtPass.Text);
                Form1 frm = (Form1)this.MdiParent;
                frm.MostrarUsuarioLogueado();

                this.Close();         
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }
    }
}
