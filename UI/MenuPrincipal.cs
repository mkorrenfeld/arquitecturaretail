﻿using BLL;
using Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE.Permisos;

namespace UI
{
    public partial class Form1 : Form
    {
        SesionBLL _sesionBLL;
        public Form1()
        {
            InitializeComponent();
            _sesionBLL = new SesionBLL();
            MostrarUsuarioLogueado();
        }

        private void iniciarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmLogin frm = new frmLogin();
            frm.MdiParent = this;
            frm.Show();
        }

        public void MostrarUsuarioLogueado() 
        {
            if (Singleton.IsLogged())
            {
                lblUsuarioLogin.Text = _sesionBLL.devolverNombreSesion();
                lblUsuarioInicio.Text = Singleton.getInstance.InicioSesion.ToString();
                this.itemCerrarSesion.Enabled = true;
                this.itemIniciarSesion.Enabled = false;
            }
            else
            {
                lblUsuarioLogin.Text = "[Sesión no iniciada]";
                lblUsuarioInicio.Text = DateTime.Now.ToString();
                this.itemCerrarSesion.Enabled = false;
                this.itemIniciarSesion.Enabled = true;
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void cerrarAplicaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void itemCerrarSesion_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro que quiere cerrar sesión?", "Confirme", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                _sesionBLL.Logout();
                MostrarUsuarioLogueado();
            }
        }

        private void ventasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (_sesionBLL.IsInRole(TipoPermiso.Consultar))                
            {
                MessageBox.Show("Bienvenido, tiene permisos");
            }
            else
            {
                MessageBox.Show("No tiene permisos, largo de aquí");
            }
        }
    }
}
